/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2017 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.jasperreports;

import net.sf.jasperreports.components.list.DesignListContents;
import net.sf.jasperreports.components.list.ListComponent;
import net.sf.jasperreports.components.table.BaseColumn;
import net.sf.jasperreports.components.table.Cell;
import net.sf.jasperreports.components.table.DesignCell;
import net.sf.jasperreports.components.table.GroupCell;
import net.sf.jasperreports.components.table.StandardColumn;
import net.sf.jasperreports.components.table.StandardTable;
import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRComponentElement;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRElementGroup;
import net.sf.jasperreports.engine.JRFont;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.component.Component;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JasperDesign;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Scales {@code JasperDesign}s.
 * <p>
 * This was originally based on Roger Diller's (<roger@flexrentalsolutions.com>) JasperDesignScaleUtils at
 * https://github.com/flex-rental-solutions/jasper-reports-scaler
 * <p>
 * It adds support for height and font scaling, and prevents overlaps in scaled fields where fields boundaries touch.
 */
public class JasperDesignScaler {

    /**
     * Scales the supplied design to the new paper size.
     *
     * @param design       the design to scale
     * @param paperSize    the paper size
     * @param scaleFonts   if {@code true}, scale fonts, otherwise leave them unchanged
     * @param scaleMargins if {@code true}, scale margins, otherwise leave them unchanged
     */
    public void scale(JasperDesign design, PaperSize paperSize, boolean scaleFonts, boolean scaleMargins) {
        int oldWidth = design.getPageWidth();
        int oldHeight = design.getPageHeight();

        switch (design.getOrientationValue()) {
            case PORTRAIT:
                design.setPageWidth(paperSize.getWidth());
                design.setPageHeight(paperSize.getHeight());
                break;
            case LANDSCAPE:
                design.setPageWidth(paperSize.getHeight());
                design.setPageHeight(paperSize.getWidth());
                break;
        }

        int newWidth = design.getPageWidth();
        int newHeight = design.getPageHeight();

        double widthRatio;
        double heightRatio;
        int columnWidth;
        int leftMargin;
        int rightMargin;
        int topMargin;
        int bottomMargin;

        int oldLeftMargin = design.getLeftMargin();
        if (scaleMargins) {
            widthRatio = newWidth / (double) oldWidth;
            heightRatio = newHeight / (double) oldHeight;
            columnWidth = round(design.getColumnWidth() * widthRatio);
            if (oldLeftMargin == design.getRightMargin()) {
                // try and avoid rounding error
                rightMargin = (newWidth - columnWidth) / 2;
            } else {
                rightMargin = round(design.getRightMargin() * widthRatio);
            }
            leftMargin = newWidth - columnWidth - rightMargin;
            topMargin = round(design.getTopMargin() * heightRatio);
            bottomMargin = round(design.getBottomMargin() * heightRatio);
        } else {
            leftMargin = oldLeftMargin;
            rightMargin = design.getRightMargin();
            topMargin = design.getTopMargin();
            bottomMargin = design.getBottomMargin();
            int marginX = leftMargin + rightMargin;
            int marginY = topMargin + bottomMargin;
            columnWidth = newWidth - marginX;
            widthRatio = columnWidth / (double) (oldWidth - marginX);
            heightRatio = (newHeight - marginY) / (double) (oldHeight - marginY);
        }

        int columnSpacing = round(design.getColumnSpacing() * widthRatio);

        design.setColumnWidth(columnWidth);
        design.setColumnSpacing(columnSpacing);
        design.setLeftMargin(leftMargin);
        design.setRightMargin(rightMargin);
        design.setTopMargin(topMargin);
        design.setBottomMargin(bottomMargin);

        if (scaleFonts) {
            for (JRStyle style : design.getStyles()) {
                scale(style, heightRatio);
            }
        }

        List<JRBand> bands = new ArrayList<>(Arrays.asList(design.getAllBands()));
        if (design.getBackground() != null) {
            bands.add(design.getBackground());
        }
        int marginY = topMargin + bottomMargin;
        for (JRBand band : bands) {
            Integer height = null;
            if (band instanceof JRDesignBand) {
                double scaledHeight = band.getHeight() * heightRatio;
                height = new BigDecimal(scaledHeight).setScale(0, RoundingMode.UP).intValue();
                if (height + marginY > design.getPageHeight()) {
                    height = design.getPageHeight() - marginY;
                }
                ((JRDesignBand) band).setHeight(height);
            }
            Map<Integer, Integer> xOffsets = new HashMap<>();
            Map<Integer, Integer> yOffsets = new HashMap<>();
            scaleOffsets(band, xOffsets, yOffsets, widthRatio, heightRatio);

            for (JRElement element : band.getElements()) {
                scale(element, widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets, height);
            }
        }
    }

    /**
     * Collects and scales all of the x and y offsets in a band.
     *
     * @param band        the band
     * @param x           the x offsets and their corresponding scaled offsets
     * @param y           the y offsets and their corresponding scaled offsets
     * @param widthRatio  the width ratio
     * @param heightRatio the height ratio
     */
    private void scaleOffsets(JRBand band, Map<Integer, Integer> x, Map<Integer, Integer> y, double widthRatio,
                              double heightRatio) {
        for (JRElement element : band.getElements()) {
            scaleOffsets(element, x, y, widthRatio, heightRatio);
        }
    }

    /**
     * Collects and scales all of the x and y offsets in an element.
     *
     * @param element     the element
     * @param x           the x offsets and their corresponding scaled offsets
     * @param y           the y offsets and their corresponding scaled offsets
     * @param widthRatio  the width ratio
     * @param heightRatio the height ratio
     */
    private void scaleOffsets(JRElement element, Map<Integer, Integer> x, Map<Integer, Integer> y, double widthRatio,
                              double heightRatio) {
        x.put(element.getX(), round(element.getX() * widthRatio));
        y.put(element.getY(), round(element.getY() * heightRatio));
        JRElement[] children = null;
        if (element instanceof JRElementGroup) {
            children = ((JRElementGroup) element).getElements();
        } else if (element instanceof JRComponentElement) {
            Component component = ((JRComponentElement) element).getComponent();
            if (component instanceof ListComponent) {
                DesignListContents listContents = (DesignListContents) ((ListComponent) component).getContents();
                listContents.setWidth(element.getWidth());
                children = ((ListComponent) component).getContents().getElements();
            } else if (component instanceof StandardTable) {
                StandardTable table = (StandardTable) component;
                for (BaseColumn column : table.getColumns()) {
                    if (column instanceof StandardColumn) {
                        scaleOffsets((StandardColumn) column, x, y, widthRatio, heightRatio);
                    }
                }
            }
        }
        if (children != null) {
            for (JRElement child : children) {
                scaleOffsets(child, x, y, widthRatio, heightRatio);
            }
        }
    }

    /**
     * Collects and scales all of the x and y offsets in a table column.
     *
     * @param column      the column
     * @param x           the x offsets and their corresponding scaled offsets
     * @param y           the y offsets and their corresponding scaled offsets
     * @param widthRatio  the width ratio
     * @param heightRatio the height ratio
     */
    private void scaleOffsets(StandardColumn column, Map<Integer, Integer> x, Map<Integer, Integer> y,
                              double widthRatio, double heightRatio) {
        scaleOffsets(column.getColumnHeader(), x, y, widthRatio, heightRatio);
        scaleOffsets(column.getDetailCell(), x, y, widthRatio, heightRatio);
        scaleOffsets(column.getColumnFooter(), x, y, widthRatio, heightRatio);
        for (GroupCell header : column.getGroupHeaders()) {
            scaleOffsets(header.getCell(), x, y, widthRatio, heightRatio);
        }
        for (GroupCell footer : column.getGroupFooters()) {
            scaleOffsets(footer.getCell(), x, y, widthRatio, heightRatio);
        }
    }

    /**
     * Collects and scales all of the x and y offsets in a table cell.
     *
     * @param cell        the cell. May be {@code null}
     * @param x           the x offsets and their corresponding scaled offsets
     * @param y           the y offsets and their corresponding scaled offsets
     * @param widthRatio  the width ratio
     * @param heightRatio the height ratio
     */
    private void scaleOffsets(Cell cell, Map<Integer, Integer> x, Map<Integer, Integer> y,
                              double widthRatio, double heightRatio) {
        if (cell != null) {
            for (JRElement element : cell.getElements()) {
                scaleOffsets(element, x, y, widthRatio, heightRatio);
            }
        }
    }

    /**
     * Scales an element and its children.
     *
     * @param element     the element to scale
     * @param widthRatio  the width scale ratio
     * @param heightRatio the height scale ratio
     * @param scaleFonts  if {@code true}, scale fonts, otherwise leave them unchanged
     * @param xOffsets    a map of x field offsets to their scaled offsets
     * @param yOffsets    a map of y field offsets to their scaled offsets
     * @param height      the height of the parent element, or {@code null} if it is not known
     */
    protected void scale(JRElement element, double widthRatio, double heightRatio, boolean scaleFonts,
                         Map<Integer, Integer> xOffsets, Map<Integer, Integer> yOffsets, Integer height) {
        int oldX = element.getX();
        int oldY = element.getY();
        int newX = round(oldX * widthRatio);
        int newY = round(oldY * heightRatio);
        int oldWidth = element.getWidth();
        int oldHeight = element.getHeight();
        int right = oldX + oldWidth;
        int bottom = oldY + oldHeight;
        int newRight = scale(xOffsets, right, widthRatio);
        int newBottom = scale(yOffsets, bottom, heightRatio);
        if (height != null && newBottom > height) {
            // clip the element, if it exceeds the height of its parent
            newBottom = height;
        }

        int newWidth = newRight - newX;
        int newHeight = newBottom - newY;

        if (element instanceof JRDesignElement) {
            ((JRDesignElement) element).setHeight(newHeight);
            ((JRDesignElement) element).setY(newY);
        }
        element.setWidth(newWidth);
        element.setX(newX);
        if (scaleFonts && element instanceof JRFont) {
            scale((JRFont) element, heightRatio);
        }

        double elementWidthRatio = newWidth / (double) oldWidth;
        double elementHeightRatio = newHeight / (double) oldHeight;

        if (element instanceof JRElementGroup) {
            JRElement[] children = ((JRElementGroup) element).getElements();
            scale(children, elementWidthRatio, elementHeightRatio, scaleFonts, xOffsets, yOffsets, newHeight);
        } else if (element instanceof JRComponentElement) {
            Component component = ((JRComponentElement) element).getComponent();
            if (component instanceof ListComponent) {
                DesignListContents listContents = (DesignListContents) ((ListComponent) component).getContents();
                listContents.setWidth(element.getWidth());
                JRElement[] children = ((ListComponent) component).getContents().getElements();
                scale(children, elementWidthRatio, elementHeightRatio, scaleFonts, xOffsets, yOffsets, newHeight);
            } else if (component instanceof StandardTable) {
                StandardTable table = (StandardTable) component;
                scaleTable(table, elementWidthRatio, elementHeightRatio, scaleFonts, xOffsets, yOffsets);
            }
        }
    }

    /**
     * Scales elements and their children.
     *
     * @param elements    the elements to scale
     * @param widthRatio  the width scale ratio
     * @param heightRatio the height scale ratio
     * @param scaleFonts  if {@code true}, scale fonts, otherwise leave them unchanged
     * @param xOffsets    a map of x field offsets to their scaled offsets
     * @param yOffsets    a map of y field offsets to their scaled offsets
     * @param height      the available height, or {@code null} if it is not known
     */
    private void scale(JRElement[] elements, double widthRatio, double heightRatio, boolean scaleFonts,
                       Map<Integer, Integer> xOffsets, Map<Integer, Integer> yOffsets, Integer height) {
        for (JRElement element : elements) {
            scale(element, widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets, height);
        }
    }

    /**
     * Scales an offset, using an existing offset if one is present.
     * <p>
     * This ensures that scaled fields don't overlap other fields when their calculated right or bottom edge aligns
     * to another field.
     *
     * @param offsets the map of offsets to their scaled values
     * @param offset  the offset to scale
     * @param ratio   the scale ratio
     * @return the scaled offset
     */
    private int scale(Map<Integer, Integer> offsets, int offset, double ratio) {
        int result;
        Integer newOffset = offsets.get(offset);
        if (newOffset == null) {
            result = round(offset * ratio);
        } else {
            result = newOffset;
        }
        return result;
    }

    /**
     * Scales a table.
     *
     * @param table       the table to scale
     * @param widthRatio  the width scale ratio
     * @param heightRatio the height scale ratio
     * @param scaleFonts  if {@code true}, scale fonts, otherwise leave them unchanged
     * @param xOffsets    a map of x field offsets to their scaled offsets
     * @param yOffsets    a map of y field offsets to their scaled offsets
     */
    private void scaleTable(StandardTable table, double widthRatio, double heightRatio,
                            boolean scaleFonts, Map<Integer, Integer> xOffsets, Map<Integer, Integer> yOffsets) {
        for (BaseColumn column : table.getColumns()) {
            if (column instanceof StandardColumn) {
                scaleColumn((StandardColumn) column, widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
            }
        }
    }

    /**
     * Scales a table column.
     *
     * @param column      the column to scale
     * @param widthRatio  the width scale ratio
     * @param heightRatio the height scale ratio
     * @param scaleFonts  if {@code true}, scale fonts, otherwise leave them unchanged
     * @param xOffsets    a map of x field offsets to their scaled offsets
     * @param yOffsets    a map of y field offsets to their scaled offsets
     */
    private void scaleColumn(StandardColumn column, double widthRatio, double heightRatio,
                             boolean scaleFonts, Map<Integer, Integer> xOffsets,
                             Map<Integer, Integer> yOffsets) {
        Integer width = column.getWidth();
        if (width != null) {
            column.setWidth(round(width * widthRatio));
        }
        scaleCell(column.getColumnHeader(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
        scaleCell(column.getDetailCell(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
        scaleCell(column.getColumnFooter(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
        for (GroupCell header : column.getGroupHeaders()) {
            scaleCell(header.getCell(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
        }
        for (GroupCell footer : column.getGroupFooters()) {
            scaleCell(footer.getCell(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets);
        }
    }

    /**
     * Scales a table cell.
     *
     * @param cell        the cell to scale. May be {@code null}
     * @param widthRatio  the width scale ratio
     * @param heightRatio the height scale ratio
     * @param scaleFonts  if {@code true}, scale fonts, otherwise leave them unchanged
     * @param xOffsets    a map of x field offsets to their scaled offsets
     * @param yOffsets    a map of y field offsets to their scaled offsets
     */
    private void scaleCell(Cell cell, double widthRatio, double heightRatio, boolean scaleFonts,
                           Map<Integer, Integer> xOffsets, Map<Integer, Integer> yOffsets) {
        if (cell instanceof DesignCell) {
            Integer height = cell.getHeight();
            if (height != null) {
                height = round(height * heightRatio);
                ((DesignCell) cell).setHeight(height);
            }
            scale(cell.getElements(), widthRatio, heightRatio, scaleFonts, xOffsets, yOffsets, height);
        }
    }

    /**
     * Scales a font element.
     *
     * @param element the element to scale
     * @param ratio   the scale ratio
     */
    protected void scale(JRFont element, double ratio) {
        Float size = element.getOwnFontsize();
        if (size != null) {
            element.setFontSize(scaleFont(size, ratio));
        }
    }

    /**
     * Scales a font in a style.
     *
     * @param ratio the scale ratio
     * @param style the style to scale
     */
    protected void scale(JRStyle style, double ratio) {
        Float size = style.getOwnFontsize();
        if (size != null) {
            style.setFontSize(scaleFont(size, ratio));
        }
    }

    /**
     * Scales a font size down.
     *
     * @param size  the font size
     * @param ratio the ratio
     * @return the rounded value
     */
    private float scaleFont(float size, double ratio) {
        return new BigDecimal(size * ratio).setScale(0, RoundingMode.DOWN).floatValue();
    }

    /**
     * Helper to round a double value down.
     *
     * @param value the value to round
     * @return the rounded value
     */
    private int round(double value) {
        return new BigDecimal(value).setScale(0, RoundingMode.HALF_UP).intValue();
    }

}
