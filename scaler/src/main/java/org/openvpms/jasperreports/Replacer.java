/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2016 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.jasperreports;

import net.sf.jasperreports.engine.JRStaticText;
import net.sf.jasperreports.engine.JRTextField;
import net.sf.jasperreports.engine.util.JRVisitorSupport;

import java.util.Properties;

/**
 * Replaces static text and patterns in a JasperReport.
 * <p>
 * Replacement is driven by a properties file. E.g.:
 * <pre>
 * replace.pattern.1 = dd/MM/yyyy
 * with.pattern.1 = MM/dd/yyyy
 * replace.pattern.2 = dd/MM
 * with.pattern.2 = MM/dd
 *
 * replace.text.1=TOTAL includes GST
 * with.text.1=TOTAL includes state tax
 * </pre>
 * In the above, all instances of:
 * <ul>
 * <li>pattern <em>dd/MM/yyyy</em> are replaced with <em>MM/dd/yyyy</em></li>
 * <li>pattern <em>dd/MM</em> are replaced with <em>MM/dd</em></li>
 * <li>text <em>TOTAL includes GST</em> are replaced with <em>TOTAL includes state tax</em></li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class Replacer extends JRVisitorSupport {

    /**
     * The properties.
     */
    private final Properties properties;

    /**
     * Constructs a {@link Replacer}.
     *
     * @param properties the
     */
    public Replacer(Properties properties) {
        this.properties = properties;
    }

    /**
     * Replaces any pattern in a text field, if there is a corresponding match in the properties.
     *
     * @param field the text field
     */
    @Override
    public void visitTextField(JRTextField field) {
        String pattern = field.getPattern();
        if (pattern != null) {
            String text = getReplacement("pattern", pattern);
            if (text != null) {
                field.setPattern(text);
            }
        }
    }

    /**
     * Replaces any text in a static text field, if there is a corresponding match in the properties.
     *
     * @param field the static text field
     */
    @Override
    public void visitStaticText(JRStaticText field) {
        String text = field.getText();
        if (text != null) {
            String value = getReplacement("text", text);
            if (value != null) {
                field.setText(value);
            }
        }
    }

    /**
     * Returns replacement text.
     *
     * @param type  one of {@code "text"} or {@code "pattern"}
     * @param value the value to replace
     * @return the replacement value, or {@code null} if there is no match
     */
    private String getReplacement(String type, String value) {
        int i = 1;
        Object srcValue;
        while ((srcValue = properties.get("replace." + type + "." + i)) != null) {
            if (srcValue.equals(value)) {
                Object o = properties.get("with." + type + "." + i);
                return o != null ? o.toString() : null;
            }
            ++i;
        }
        return null;
    }
}
