/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.jasperreports;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRVisitor;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRElementsVisitor;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.xml.JRXmlWriter;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * JasperReports scaler.
 *
 * @author Tim Anderson
 */
public class Scaler {

    /**
     * Scales a JRXML file.
     *
     * @param input        the input file
     * @param output       the output file. If {@code null}, the output is written to stdout
     * @param size         the paper size
     * @param scaleFonts   if {@code true}, scale fonts
     * @param scaleMargins if {@code true}, scale margins
     * @param properties   properties to replace text. May be {@code null}
     * @param updateInput  if {@code true}, update the input file to the latest JasperReports version if required
     * @throws JRException for any JasperReports error
     * @throws IOException for any I/O error
     */
    public void scale(File input, File output, PaperSize size, boolean scaleFonts, boolean scaleMargins,
                      Properties properties, boolean updateInput) throws JRException, IOException {
        JasperDesign design = JRXmlLoader.load(input);
        if (updateInput) {
            writeIfChanged(design, input);
        }
        JasperDesignScaler scaler = new JasperDesignScaler();
        scaler.scale(design, size, scaleFonts, scaleMargins);
        if (properties != null) {
            JRVisitor visitor = new Replacer(properties);
            JRElementsVisitor.visitReport(design, visitor);
        }
        if (output == null) {
            System.out.println(JRXmlWriter.writeReport(design, UTF_8.name()));
        } else {
            if (output.exists()) {
                writeIfChanged(design, output);
            } else {
                try (FileOutputStream stream = new FileOutputStream(output)) {
                    JRXmlWriter.writeReport(design, stream, UTF_8.name());
                }
            }
        }
    }

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("A4", false, "scale to A4");
        options.addOption("A5", false, "scale to A5");
        options.addOption("LETTER", false, "scale to US-LETTER");
        options.addOption("u", false, "update original. Use to bring source up-to-date with JasperReports version.");
        options.addOption("f", false, "scale fonts");
        options.addOption("m", false, "scale margins");
        options.addOption("o", false, "overwrite existing output");
        options.addOption("r", true, "replace values from properties file");
        options.addOption("lf", false, "output with lf as the new line character. If not specified, the platform " +
                                       "default will be used.");
        options.addOption("v", false, "verbose output");
        CommandLineParser parser = new BasicParser();
        try {
            CommandLine line = parser.parse(options, args);
            PaperSize size = null;
            String[] files = line.getArgs();
            if (files.length < 1 || files.length > 2) {
                usage(options);
            } else if (line.hasOption("A4")) {
                size = PaperSize.A4;
            } else if (line.hasOption("A5")) {
                size = PaperSize.A5;
            } else if (line.hasOption("LETTER")) {
                size = PaperSize.LETTER;
            } else {
                usage(options);
            }
            boolean update = line.hasOption("u");
            boolean scaleFonts = line.hasOption("f");
            boolean scaleMargins = line.hasOption("m");
            boolean overwrite = line.hasOption("o");
            boolean verbose = line.hasOption("v");
            File[] sources;
            File input = new File(files[0]);
            if (!input.exists()) {
                abort(files[0] + " not found");
            }
            if (input.isDirectory()) {
                sources = input.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.isFile() && pathname.getName().toLowerCase().endsWith(".jrxml");
                    }
                });
                if (sources == null || sources.length == 0) {
                    abort(input + " is empty");
                }
            } else {
                sources = new File[]{input};
            }
            File output = null;
            if (files.length > 1) {
                output = new File(files[1]);
                if (sources.length > 1 && !output.isDirectory()) {
                    abort(output + " must be a directory");
                }
                for (File source : sources) {
                    File target = (output.isDirectory()) ? new File(output, source.getName()) : output;
                    if (!overwrite && target.exists()) {
                        abort(target + " exists. Use -o to overwrite it");
                    } else if (target.getCanonicalFile().equals(source.getCanonicalFile())) {
                        abort("Output file cannot be the same as the input");
                    }
                }
            }
            String replace = line.getOptionValue("r");
            Properties properties = null;
            if (replace != null) {
                File file = new File(replace);
                if (!file.exists()) {
                    abort(replace + " not found");
                }
                properties = new Properties();
                properties.load(new FileInputStream(file));
            }
            Scaler scale = new Scaler();
            for (File source : sources) {
                File target = null;
                if (output != null) {
                    if (output.isDirectory()) {
                        target = new File(output, source.getName());
                    } else {
                        target = output;
                    }
                }
                if (verbose) {
                    System.out.print("Scaling " + source.getPath());
                    if (target != null) {
                        System.out.println(" -> " + target.getPath());
                    } else {
                        System.out.println();
                    }
                }
                scale.scale(source, target, size, scaleFonts, scaleMargins, properties, update);
            }
            System.exit(0);
        } catch (Exception exception) {
            exception.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Writes a JasperReport to the output file if it has changed.
     *
     * @param design the report
     * @param output the output file
     * @throws JRException for any JasperReport error
     * @throws IOException for any I/O error
     */
    private void writeIfChanged(JasperDesign design, File output) throws JRException, IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        JRXmlWriter.writeReport(design, bytes, UTF_8.name());

        byte[] buffer = bytes.toByteArray();
        if (changed(output, buffer)) {
            try (FileOutputStream stream = new FileOutputStream(output)) {
                IOUtils.write(buffer, stream);
            }
        }
    }

    /**
     * Determines if a report has changed, ignoring any line endings.
     *
     * @param file   the report file
     * @param buffer the buffer of the scaled report
     * @return {@code true} if the file has changed
     * @throws IOException for any I/O error
     */
    private boolean changed(File file, byte[] buffer) throws IOException {
        boolean changed = false;
        BufferedReader old = new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
        BufferedReader now = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer), UTF_8));
        String line1;
        String line2;
        do {
            line1 = old.readLine();
            line2 = now.readLine();

            // strip out UUIDs to avoid spurious changes
            line1 = removeUUID(line1);
            line2 = removeUUID(line2);
            if (!Objects.equals(line1, line2)) {
                changed = true;
                break;
            }
        } while (line1 != null);
        return changed;
    }

    /**
     * Strips any uuid=".." from a line, as these aren't important for comparison purposes.
     *
     * @param line the line. May be {@code null}
     * @return the line with uuids removed
     */
    private String removeUUID(String line) {
        if (line != null) {
            line = line.replaceAll("uuid=\"[^\"]*\"", "");
        }
        return line;
    }

    private static void abort(String message) {
        System.err.println(message);
        System.exit(1);
    }

    private static void usage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("usage: scale [options] <input> [output]", options);
        System.out.println(" input      may be a single .jrxml file, or a directory");
        System.out.println(" output     may be a single .jrxml file, or a directory, or unspecified.");
        System.out.println("            If unspecified, the scaled version will be output to the console");
        System.exit(1);
    }

}
